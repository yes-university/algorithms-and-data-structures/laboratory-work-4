import yargs from "yargs";
import { faker } from "@faker-js/faker/locale/uk";
import bootstrap from "../utils";

interface Poetry {
    name: string;
    author: string;
    consumedTime: number;
}

interface Music {
    name: string;
    author: string;
    composer: string;
    consumedTime: number;
    genre: string;
}

const poetry: Poetry[] = [];
const music: Music[] = [];

async function performInput(text: string[]): Promise<any> {
    const performedText = text[0] === "bob" ? text.slice(1) : text;
    return yargs(!performedText.length ? ["--help"] : performedText)
        .scriptName("bob")
        .version("default")
        .usage("$0 <cmd> [args]")
        .command("clear", "Clear console", {}, () => {
            console.clear();
        })
        .command(
            "add-poetry",
            "Add new poetry",
            {
                name: {
                    alias: "n",
                    description: "Name of the poetry",
                    required: true,
                    type: "string",
                },
                author: {
                    alias: "a",
                    description: "Author of the poetry",
                    required: true,
                    type: "string",
                },
                "consumed-time": {
                    alias: "c",
                    description: "Consumed time of the poetry (in minutes)",
                    required: true,
                    type: "number",
                },
            },
            (args) => {
                poetry.push({
                    name: args.name,
                    author: args.author,
                    consumedTime: args["consumed-time"],
                });
            }
        )
        .command(
            "add-music",
            "Add new music",
            {
                name: {
                    alias: "n",
                    description: "Name of music",
                    required: true,
                    type: "string",
                },
                author: {
                    alias: "a",
                    description: "Author of music",
                    required: true,
                    type: "string",
                },
                composer: {
                    alias: "cps",
                    description: "Composer of music",
                    required: true,
                    type: "string",
                },
                genre: {
                    alias: "g",
                    description: "Genre of music",
                    required: true,
                    type: "string",
                },
                "consumed-time": {
                    alias: "c",
                    description: "Consumed time of music (in minutes)",
                    required: true,
                    type: "number",
                },
            },
            (args) => {
                music.push({
                    name: args.name,
                    author: args.author,
                    composer: args.composer,
                    genre: args.genre,
                    consumedTime: args["consumed-time"],
                });
            }
        )
        .command(
            "fill-poetry",
            "Quickly fill poetry with random data (faker)",
            {
                amount: {
                    alias: "c",
                    description: "How many poetry to add",
                    default: 10,
                    type: "number",
                },
            },
            (args) => {
                for (let i = 0; i < args.amount; i++) {
                    poetry.push({
                        name: faker.music.songName(),
                        author: `${faker.person.middleName()} ${faker.person.firstName()}`,
                        consumedTime: faker.number.int({ min: 1, max: 100 }),
                    });
                }
                console.log("Poetry:", poetry);
            }
        )
        .command(
            "fill-music",
            "Quickly fill music with random data (faker)",
            {
                amount: {
                    alias: "c",
                    description: "How many poetry to add",
                    default: 10,
                    type: "number",
                },
            },
            (args) => {
                for (let i = 0; i < args.amount; i++) {
                    music.push({
                        name: faker.music.songName(),
                        author: `${faker.person.middleName()} ${faker.person.firstName()}`,
                        composer: `${faker.person.middleName()} ${faker.person.firstName()}`,
                        genre: faker.music.genre(),
                        consumedTime: faker.number.int({ min: 1, max: 100 }),
                    });
                }
                console.log("Music:", music);
            }
        )
        .command(
            "music-featured-genre",
            "Get featured genre of music",
            {},
            () => {
                const genres = music
                    .map((m) => m.genre)
                    .reduce((acc, cur) => {
                        if (acc[cur]) {
                            acc[cur]++;
                        } else {
                            acc[cur] = 1;
                        }
                        return acc;
                    }, {} as { [key: string]: number });
                const max = Math.max(...Object.values(genres));
                const featuredGenre = Object.entries(genres).filter(
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    ([_, value]) => value === max
                )[0];
                console.log("Featured genre:", featuredGenre);
            }
        )
        .command(
            "poetry-featured-author",
            "Get featured author of poetry",
            {},
            () => {
                const authors = poetry
                    .map((p) => p.author)
                    .reduce((acc, cur) => {
                        if (acc[cur]) {
                            acc[cur]++;
                        } else {
                            acc[cur] = 1;
                        }
                        return acc;
                    }, {} as { [key: string]: number });
                const max = Math.max(...Object.values(authors));
                const featuredAuthor = Object.entries(authors).filter(
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    ([_, value]) => value === max
                )[0];
                console.log("Featured author:", featuredAuthor);
            }
        )
        .command(
            "is-poetry-authors-presented-in-music",
            "Check if poetry authors presented in music authors",
            {},
            () => {
                const poetryAuthors = poetry.map((p) => p.author);
                const musicAuthors = music.map((m) => m.author);
                const isPresented = poetryAuthors.some((pa) =>
                    musicAuthors.includes(pa)
                );
                if (isPresented) {
                    console.log("Poetry authors presented in music authors");
                    const presentedAuthors = poetryAuthors.filter((pa) =>
                        musicAuthors.includes(pa)
                    );
                    console.log("Presented authors:", presentedAuthors);
                } else {
                    console.log(
                        "Poetry authors not presented in music authors"
                    );
                }
            }
        )
        .command(
            "average-consumed-time",
            "Get average consumed time of poetry and music",
            {},
            () => {
                const poetryConsumedTime = poetry.map((p) => p.consumedTime);
                const musicConsumedTime = music.map((m) => m.consumedTime);
                const averageConsumedTime =
                    (poetryConsumedTime.reduce((acc, cur) => acc + cur, 0) +
                        musicConsumedTime.reduce((acc, cur) => acc + cur, 0)) /
                    (poetryConsumedTime.length + musicConsumedTime.length);
                console.log("Average consumed time:", averageConsumedTime);
            }
        )
        .help()
        .exitProcess(false)
        .fail((msg, err, y) => {
            console.error(msg);
            if (err) throw err;
            y.showHelp();
        })
        .showHelpOnFail(true)
        .parse();
}

bootstrap(performInput, "bob");
