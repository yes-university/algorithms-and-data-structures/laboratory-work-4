import readline from "readline";

const bootstrap = async (executor: Function, name?: string) => {
    const title = name || "app";
    const readLineData = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    while (true) {
        const shouldStop = await new Promise(async (resolve: any) => {
            readLineData.question(
                `Perform command like "${title} --help" or type "exit":\n`,
                async (data: string) => {
                    if (data === "exit") {
                        resolve(true);
                    } else {
                        await executor(data.split(" "));
                        setTimeout(() => {
                            resolve(false);
                        }, 1000);
                    }
                }
            );
        });
        if (shouldStop) break;
    }
    readLineData.close();
};

export default bootstrap;
